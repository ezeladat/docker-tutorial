BASE='jenkins_home'
MODULE='docker-monitor'
DIR=$BASE/$MODULE
BRANCH='ezt-srv1'
if [ -d "$DIR" ]; then
  echo 'Enter a directory '$DIR'...'
  cd $DIR
  docker-compose stop
  ifconfig
  id
  uname -a
  pwd
  ls -latr
  git pull -f
#  git submodule update --init
#  git submodule sync
#  git submodule foreach git pull -f origin inno
  pwd
  ls -latr
  docker-compose up -d
  exit
else
  echo 'Creating a directory '$DIR'...'
  cd $BASE
  ifconfig
  id
  uname -a
  pwd
  ls -latr
  git clone -b $BRANCH --depth 1 --recurse-submodules 'https://ezeladat:glpat-hzCF7qGz5ZQ4VfNfxswQ@gitlab.com/ezeladat/docker-monitor.git'
  cd $MODULE
  pwd
  ls -latr
  docker-compose up -d
  exit
fi

