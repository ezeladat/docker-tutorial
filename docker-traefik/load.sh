BASE='jenkins_home'
MODULE='docker-traefik'
DIR=$BASE/$MODULE
if [ -d "$DIR" ]; then
  echo 'Enter a directory '$DIR'...'
  cd $DIR
  docker-compose stop
  ifconfig
  id
  uname -a
  pwd
  ls -latr
  git pull -f
  pwd
  ls -latr
  docker-compose up -d
  exit
else
  echo 'Creating a directory '$DIR'...'
  cd $BASE
  ifconfig
  id
  uname -a
  pwd
  ls -latr
  git clone -b ezt-svr1 --depth 1 'https://ezeladat:glpat-hzCF7qGz5ZQ4VfNfxswQ@gitlab.com/ezeladat/docker-traefik.git'
  cd $MODULE
  pwd
  ls -latr
  docker-compose up -d
  exit
fi

